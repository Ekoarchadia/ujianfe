import { Injectable } from '@angular/core';
import { Http} from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch'; 

@Injectable()
export class DataService {

  constructor(private http:Http) { }

  UserList:object[];

  getData(){
    return this.http
    .get('https://jsonplaceholder.typicode.com/users')
    .map(result => result.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error");

  }
addUserList(obj:object):object[]
  {
    this.UserList.push(obj);
    return this.UserList;
  }

}