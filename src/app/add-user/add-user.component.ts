import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private api:DataService) { }

  UserList: object[];

  ngOnInit() {
    this.api.UserList = this.UserList;
  }

  name: string="";
  email: string="";
  address: string="";
  phone: number;
  company: string="";

    
    addUser(){
    this.api.addUserList({
      "name":this.name,
      "email":this.email,
      "address":this.address,
      "phone":this.phone,
      "company":this.company,
    });
  }
  }


