import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private api:DataService) { }
  
  UserList: object[];
  UserData : object[];

  ngOnInit() {
    this.api.getData()
    .subscribe(result => this.UserList = result);
    
    
    this.api.UserList = this.UserData;
  }

  remove(index){
    this.UserList.splice(index,1);
  }

}
